from __future__ import print_function
from Bio import SeqIO
import csv
import os
import argparse
import sys
from collections import defaultdict
from collections import namedtuple
import subprocess
import fnmatch


def parse_args():
    parser = argparse.ArgumentParser(description='Runs blast for all files in input directory')
    parser.add_argument('--genes_file', type=str, default='genes.txt', help='File with genes\' positions')
    parser.add_argument('--input_dir', type=str, default='blast_filtered/', help='directory with filtered blast alignments')
    parser.add_argument('--sequence_dir', type=str, default='sequences/', help='directory with sequences')
    parser.add_argument('--output_dir', type=str, default='primer_input/', help='output directory')
    return parser.parse_args()

GeneInfo = namedtuple('GeneInfo', ['chrom', 'name', 'start', 'end'])

def read_genes_position (genes_filename):
    # result = map: chr -> list(gene_name * start * end)
    result = defaultdict(list)
    # filtering repeating genes
    duplicate_sieve = dict()
    with open(genes_filename, 'r') as genes_file:
        genes_mapping = csv.DictReader(genes_file, dialect='excel-tab')
        for gene_record in genes_mapping:
            gene_info = GeneInfo(gene_record['chrom'], gene_record['#name'],
                    gene_record['txStart'], gene_record['txEnd'])
            if gene_info.name in duplicate_sieve:
                print("Removing duplicate.", file=sys.stderr)
                prev_gene_split = duplicate_sieve[gene_info.name]
                if (prev_gene_split.end - prev_gene_split.start <
                        gene_info.end - gene_info.start):
                        duplicate_sieve[gene_info.name] = gene_info
            else:
                duplicate_sieve[gene_info.name] = gene_info
    print("Genes count: {0}.".format(len(duplicate_sieve)), file=sys.stderr)
    for gene_info in duplicate_sieve.values():
        result[gene_info.chrom].append(gene_info)
    return result

def parse_name_into_range (seq_name):
    seq_range_string = seq_name.split(" ")[0]
    seq_range = (int(x) for x in seq_range_string.split("_"))
    return seq_range

def check_for_gene_intersection(alignment_range, gene_info):
    return (max(alignment_range[0], gene_info.start) <
            min(alignment_range[1], gene_info.end))

def prepare_primer_input(alignments_path, seq_path, output_path, genes):
    with (open(alignemts_path, 'r') as alignments,
            open(seq_path, 'r') as seqs,
            open(output_path, 'w') as output):
        rows = csv.reader(alignments, delim=',')
        chrseq = SeqIO.parse(seqs, 'fasta').next()
        for row in rows:
            if row[5] == '1':
                # We're only interested in opposing matches
                continue
            range1 = parse_name_into_range(row[0])
            range2 = parse_name_into_range(row[1])
            for gene in genes:
                if (check_for_gene_intersection(range1, gene) or
                    check_for_gene_intersection(range2, gene)):
                    seq_id = gene.name



def iterate_input_files (input_dir, output_dir, seq_dir, genes):
    try:
        os.mkdir(output_dir)
    except OSError:
        pass
    for chr_line_filename in os.listdir(input_dir):
        chr_path = os.path.join(input_dir, chr_line_filename)
        chr_name = chr_line_filename.split('_')[0]
        print("Creaing primer input for chromosome {0} in file: {1}".format(chr_name, chr_line_filename), file=sys.stderr)
        seq_path = os.path.join(seq_dir, chr_name + '.fa')
        output_filename = chr_name + '_primer_input.txt'
        output_path = os.path.join(output_dir, output_filename)
        prepare_primer_input(chr_path, seq_path, output_path,
                genes[chr_name])


def main ():
    args = parse_args()
    genes = read_genes_position (args.genes_file)
    iterate_input_files(args.input_dir, args.output_dir, args.sequence_dir, genes)
    sys.exit(0)


if __name__ == "__main__":
    main()
