from __future__ import print_function
import gzip
from Bio.Blast import NCBIXML
import sys
import argparse
import os

def parse_args():
    parser = argparse.ArgumentParser(description='Runs blast for all files in input directory')
    parser.add_argument('--input_dir', type=str, default='blast_alignments/', help='output directory')
    parser.add_argument('--output_dir', type=str, default='blast_filtered/', help='output directory')
    return parser.parse_args()


def parse_name_into_range (seq_name):
    seq_range_string = seq_name.split(" ")[0]
    seq_range = (int(x) for x in seq_range_string.split("_"))
    return seq_range

def get_center_of_range((start, end)):
    return (start + end) / 2

def sign (x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:
        return 1

def iterate_input_files (input_dir, output_dir):
    try:
        os.mkdir(output_dir)
    except OSError:
        pass
    for chr_line_filename in os.listdir(input_dir):
        chr_path = os.path.join(input_dir, chr_line_filename)
        chr_name = chr_line_filename.split('_')[0]
        print("Looking for interesting blast alignments {0} in file: {1}".format(chr_name, chr_line_filename), file=sys.stderr)
        output_filename = chr_name + '_filtered_alignments.csv'
        output_path = os.path.join(output_dir, output_filename)
        filter_blast(chr_path, output_path)

def filter_blast(input_filename, output_filename):
    with gzip.open(input_filename) as f, open(output_filename, 'w') as out:
        blast_records = NCBIXML.parse(f)
        for blast_record in blast_records:
            for alignment in blast_record.alignments:
                if blast_record.query >= alignment.hit_def:
                    continue
                for hsp in alignment.hsps:
                    id_pct = float(hsp.identities) / float(hsp.align_length)
                    if id_pct > 0.95 and hsp.align_length > 1000:
                        query_range = tuple(parse_name_into_range(blast_record.query))
                        query_center = get_center_of_range(query_range)
                        alignment_range = tuple(parse_name_into_range(alignment.hit_def))
                        alignment_center = get_center_of_range(alignment_range)
                        direction = sign(hsp.query_end - hsp.query_start) * sign(hsp.sbjct_end - hsp.sbjct_start)
                        print("{}, {}, {}, {}, {}, {}".format (
                                blast_record.query,
                                alignment.hit_def,
                                abs(query_center - alignment_center),
                                hsp.align_length, id_pct,
                                direction), file=out)
                        break

def main ():
    args = parse_args()
    iterate_input_files(args.input_dir, args.output_dir)
    sys.exit(0)


if __name__ == "__main__":
    main()
