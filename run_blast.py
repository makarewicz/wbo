from __future__ import print_function
from Bio import SeqIO
import csv
import os
import argparse
import sys
from collections import defaultdict
import subprocess
import fnmatch


def parse_args():
    parser = argparse.ArgumentParser(description='Runs blast for all files in input directory')
    parser.add_argument('--input_dir', type=str, default='sequences_lines/', help='input directory')
    parser.add_argument('--output_dir', type=str, default='blast_alignments/', help='output directory')
    return parser.parse_args()

def iterate_input_files (input_dir, output_dir):
    try:
        os.mkdir(output_dir)
    except OSError:
        pass
    for chr_line_filename in os.listdir(input_dir):
        if not fnmatch.fnmatch(chr_line_filename, '*.fa'):
            continue
        chr_path = os.path.join(input_dir, chr_line_filename)
        chr_name = chr_line_filename.split('_')[0]
        print("Creaing blast database for chromosome {0} in file: {1}".format(chr_name, chr_line_filename), file=sys.stderr)
        subprocess.call("makeblastdb -in {0} -dbtype=nucl".format(chr_path), shell=True)
        output_filename = chr_name + '_blout.xml.gz'
        output_path = os.path.join(output_dir, output_filename)
        print("Running blast against {0} and saving results in: {1}".format(chr_name, output_filename), file=sys.stderr)
        subprocess.call("blastn -db {0} -query {0} -outfmt 5 | gzip > {1}".format(chr_path, output_path), shell=True)

def main ():
    args = parse_args()
    iterate_input_files(args.input_dir, args.output_dir)
    sys.exit(0)


if __name__ == "__main__":
    main()
