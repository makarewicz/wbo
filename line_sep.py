from __future__ import print_function
from Bio import SeqIO
import csv
import os
import argparse
import sys
from collections import defaultdict


def parse_args():
    parser = argparse.ArgumentParser(description='Cuts line sequences from chromosomes.')
    parser.add_argument('--lines', type=str, default='lines.txt', help='csv file with position of LINEs')
    parser.add_argument('--input_dir', type=str, default='sequences/', help='directory with chromosomes')
    parser.add_argument('--output_dir', type=str, default='sequences_lines/', help='output directory')
    return parser.parse_args()


def get_line_fragments (lines_filename):
    print("Parsing line csv file: {0}".format(lines_filename), file=sys.stderr)
    all_lines = defaultdict(list)
    with open(lines_filename, 'rb') as csvfile:
        table = csv.reader(csvfile, delimiter='\t')
        for row in table:
            if row[11] == "LINE":
                all_lines[row[5]].append((row[6], row[7]))
    return all_lines

def iterate_input_files (input_dir, output_dir, lines):
    try:
        os.mkdir(output_dir)
    except OSError:
        pass
    for chr_filename in os.listdir(input_dir):
        print("Seperating lines from file: {0}".format(chr_filename), file=sys.stderr)
        chr_path = os.path.join(input_dir, chr_filename)
        chr_seq = SeqIO.parse(chr_path, "fasta").next()
        output_filename = os.path.splitext(chr_filename)[0] + '_lines.fa'
        output_path = os.path.join(output_dir, output_filename)
        seqs = []
        for seq_inter in lines[chr_seq.id]:
            seq = chr_seq[int(seq_inter[0]):int(seq_inter[1])]
            seq.id = seq_inter[0] + '_' + seq_inter[1]
            seqs.append(seq)
        SeqIO.write(seqs, output_path, 'fasta')

def main ():
    args = parse_args()
    lines = get_line_fragments(args.lines)
    iterate_input_files(args.input_dir, args.output_dir, lines)
    sys.exit(0)


if __name__ == "__main__":
    main()
